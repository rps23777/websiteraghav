import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient, private router: Router) {}

  otp(mobile: any) {
    return this.http.post(`${environment.apiURL}login`, { mobile });
  }

  login(payload: any) {
    return this.http.post(`${environment.apiURL}verify/otp`, payload)
  }
  
  getToken() {
    return localStorage.getItem('token')
  }

}