import {
    HttpEvent,
    HttpHandler,
    HttpRequest,
    HttpErrorResponse,
  } from '@angular/common/http';
  import { Injectable } from '@angular/core';
  import { Observable, catchError, map, throwError } from 'rxjs';
  import { AuthService } from '../_helpers/auth.service';

  @Injectable({
    providedIn: 'root',
  })
  export class TokenInterceptorService {
    constructor(
      private authService: AuthService,
    ) {}
  
  
    intercept(
      request: HttpRequest<any>,
      next: HttpHandler
    ): Observable<HttpEvent<any>> {
      // add authorization header with jwt token if available
      const token = this.authService.getToken();
      const headers: any = {};
      if (token) {
        headers['x-access-token'] = token;
      }
      request = request.clone({
        setHeaders: headers,
      });
      return next.handle(request).pipe(
        map((event: HttpEvent<any>) => {
          return event;
        }),
        catchError((error: HttpErrorResponse) => {
          if (error.status === 401 && error.error.tokenVerification === false) {
            //remove localstorage and current user from local storage
            // this.snackBarService.error('Session expired. Please Login');
            // this.authService.logout();
          } else if (error.status === 403) {
           //remove localstorage and current user from local storage
            // this.snackBarService.error('Please Login');
            // this.authService.logout();
          }
          return throwError(error);
        })
      );
    }
  }
  