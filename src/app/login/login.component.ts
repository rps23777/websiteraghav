import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_helpers/auth.service';
import { FormControl, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  isOtp: boolean = false;
  mobile: any;
  otp: any;
  wrongOtp: boolean = false;
  wrongMobileNumber: boolean = false;
  hash: string = '';

  loginForm = this.fb.group({
    mobile: [''],
    otp: ['']
  });

  constructor(
    private authService: AuthService, private router: Router,
    private fb: FormBuilder) {

  }
  ngOnInit(): void {
  }

  submit() {
    if (this.loginForm.value.otp != '' && this.loginForm.value.mobile != '') {
      this.signIn()
    }
    else if (this.loginForm.value.otp === '') {
      this.getOtp()
    }
    else {
      console.log('please enter value.')
    }
  }
  signIn() {
    const data = {
      mobile: this.loginForm.get('mobile')?.value,
      otp: this.loginForm.get('otp')?.value,
      hash: this.hash
    }
    this.authService.login(data).subscribe((res: any) => {
      let response = res;
      //set the token in localstorage
      if (response.success) {
        localStorage.setItem('token', res.user.token);
        this.router.navigate(['/'])
      } else {
        this.wrongOtp = true;
        this.loginForm.get('otp')?.setErrors({ 'invalid': true });
        console.log("Invalid Otp");
      }
    });
  }


  getOtp() {

    const data = this.loginForm.get('mobile')?.value;
    console.log(data);
    this.authService.otp(data).subscribe(res => {
      let response: any = res;
      if (response.success) {
        this.isOtp = true;
        this.hash = response.hash;

      } else {
        this.wrongMobileNumber = true;
        console.log('Invalid number');
      }
    })
  }

}
